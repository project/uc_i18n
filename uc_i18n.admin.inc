<?php
// $Id$

/**
 * @file
 * Ubercart Internationalization administration menu items.
 *
 */


/**
 * Form to quickly translate attribute labels and descriptions.
 *
 * @ingroup forms
 * @see theme_uc_i18n_translate_attributes_form().
 * @see uc_i18n_translate_attributes_form_submit().
 */
function uc_i18n_translate_attributes_form($form_state) {
  $form = array();
  $languages = language_list();
  $language_default = language_default();

  drupal_set_title(t('Translate attributes'));
  $attributes = uc_i18n_attribute_load();

  $attribute_string = implode(", ", element_children($attributes));
  if ($attribute_string) {
    $saved_attributes = array();
    $result = db_query("SELECT * FROM {uc_i18n_attributes} WHERE aid IN (%s)", $attribute_string);
    while ($saved_attribute = db_fetch_object($result)) {
      $saved_attributes[$saved_attribute->aid][$saved_attribute->language] = $saved_attribute;
    }
  }

  foreach ($attributes as $i => $attribute) {
    $form['attributes'][$i]['aid'] = array(
      '#type' => 'hidden',
      '#value' => $i,
    );
    $form['attributes'][$i]['attribute'] = array(
      '#value' => $attribute->name,
    );
    $form['attributes'][$i]['languages'] = array('#weight' => 2);
    $form['attributes'][$i]['languages'][$language_default->language]['language'] = array(
      '#value' => $language_default->native,
    );
    $form['attributes'][$i]['languages'][$language_default->language]['label'] = array(
      '#type' => 'textfield',
      '#default_value' => empty($saved_attributes[$i][$language_default->language]->label) ? $attribute->name : $saved_attributes[$i][$language_default->language]->label,
      '#size' => 40,
    );
    $form['attributes'][$i]['languages'][$language_default->language]['description'] = array(
      '#type' => 'textfield',
      '#default_value' => empty($saved_attributes[$i][$language_default->language]->description) ? $attribute->description : $saved_attributes[$i][$language_default->language]->description,
    );
    if ($saved_attributes[$i][$language_default->language]->id) {
      $form['attributes'][$i]['languages'][$language_default->language]['id'] = array(
        '#type' => 'hidden',
        '#value' => $saved_attributes[$i][$language_default->language]->id,
      );
    }
    foreach ($languages as $j => $language) {
      if ($j != $language_default->language && $language->enabled == 1) {
        $form['attributes'][$i]['languages'][$j]['language'] = array(
          '#value' => $language->native,
        );
        $form['attributes'][$i]['languages'][$j]['label'] = array(
          '#type' => 'textfield',
          '#default_value' => empty($saved_attributes[$i][$j]->label) ? $attribute->name : $saved_attributes[$i][$j]->label,
          '#size' => 40,
        );
        $form['attributes'][$i]['languages'][$j]['description'] = array(
          '#type' => 'textfield',
          '#default_value' => empty($saved_attributes[$i][$j]->description) ? $attribute->description : $saved_attributes[$i][$j]->description,
        );
        if ($saved_attributes[$i][$j]->id) {
          $form['attributes'][$i]['languages'][$j]['id'] = array(
            '#type' => 'hidden',
            '#value' => $saved_attributes[$i][$j]->id,
          );
        }
      }
    }
  }

  if (!empty($form['attributes'])) {
    $form['attributes']['#tree'] = TRUE;
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
  }
  return $form;
}

/**
 * Format the translation table for attributes.
 *
 * @ingroup themeable
 */
function theme_uc_i18n_translate_attributes_form($form) {
  $header = array(' ', t('Label'), t('Description'));
  if (count(element_children($form['attributes'])) > 0) {
    foreach (element_children($form['attributes']) as $aid) {
      $row = array(
        drupal_render($form['attributes'][$aid]['aid']) . drupal_render($form['attributes'][$aid]['attribute']),
      );
      $rows[] = array(
        'data' => $row,
        'colspan' => 3,
        'align' => 'center'
      );
      foreach (element_children($form['attributes'][$aid]['languages']) as $language_id) {
        $row = array(
          drupal_render($form['attributes'][$aid]['languages'][$language_id]['id']) . drupal_render($form['attributes'][$aid]['languages'][$language_id]['language']),
          drupal_render($form['attributes'][$aid]['languages'][$language_id]['label']),
          drupal_render($form['attributes'][$aid]['languages'][$language_id]['description']),
        );
        $rows[] = array(
          'data' => $row,
        );
      }
      $rows[] = array(
        'data' => array(' '),
        'collspan' => 3,
      );
    }
  }
  else {
    $rows[] = array(
      array('data' => t('No translatable attributes yet.'), 'colspan' => 3),
    );
  }
  $output = theme('table', $header, $rows, array('id' => 'uc-i18n-translate-attributes-table'));
  $output .= drupal_render($form);
  return $output;
}

/**
 * @see uc_i18n_translate_attributes_form()
 */
function uc_i18n_translate_attributes_form_submit($form, &$form_state) {
  $language_default = language_default();
  foreach ($form_state['values']['attributes'] as $i => $attribute) {
    foreach ($attribute['languages'] as $j => $language) {
      // save results
      if ($j == $language_default->language) {
        db_query("UPDATE {uc_attributes} SET label = '%s', description ='%s' WHERE aid = %d", $language['label'], $language['description'], $attribute['aid']);
        // temp
        db_query("DELETE IGNORE from {uc_i18n_attributes} WHERE aid = %d AND language = '%s'", $attribute['aid'], $j);
      }
      else {
        $data = array(
          'aid' => $attribute['aid'],
          'language' => $j,
          'label' => $language['label'],
          'description' => $language['description'],
        );
        if (isset($language['id'])) {
          $data['id'] = $language['id'];
          drupal_write_record('uc_i18n_attributes', $data, 'id');
        }
        else {
          drupal_write_record('uc_i18n_attributes', $data);
        }
      }
    }
  }
}

/**
 * Form to associate attributes with products or classes.
 *
 * @ingroup forms
 * @see theme_uc_i18n_translate_options_form().
 * @see uc_i18n_translate_options_form_submit().
 */
function uc_i18n_translate_options_form($form_state, $attribute = NULL) {
  $form = array();
  $languages = language_list();
  $language_default = language_default();

  if ($attribute->aid) {
    // $attribute = uc_attribute_load($aid);
    drupal_set_title(t('Translate options for @attribute_name', array('@attribute_name' => $attribute->name)));
  }
  else {
    drupal_set_title(t('Translate attributes'));
    $attributes = uc_i18n_attribute_load();
  }

  $option_string = implode(", ", element_children($attribute->options));
  if ($option_string) {
    $saved_options = array();
    $result = db_query("SELECT * FROM {uc_i18n_options} WHERE oid IN (%s)", $option_string);
    while ($saved_option = db_fetch_object($result)) {
      $saved_options[$saved_option->oid][$saved_option->language] = $saved_option;
    }
  }

  foreach ($attribute->options as $i => $option) {
    $form['options'][$i]['oid'] = array(
      '#type' => 'hidden',
      '#value' => $i,
    );
    $form['options'][$i]['option'] = array(
      '#value' => $option->name,
    );
    $form['options'][$i]['languages'] = array('#weight' => 2);
    $form['options'][$i]['languages'][$language_default->language]['language'] = array(
      '#value' => $language_default->native,
    );
    $form['options'][$i]['languages'][$language_default->language]['name'] = array(
      '#type' => 'textfield',
      '#default_value' => empty($saved_options[$i][$language_default->language]->name) ? $option->name : $saved_options[$i][$language_default->language]->name,
    );
    if ($saved_options[$i][$language_default->language]->id) {
      $form['options'][$i]['languages'][$language_default->language]['id'] = array(
        '#type' => 'hidden',
        '#value' => $saved_options[$i][$language_default->language]->id,
      );
    }
    foreach ($languages as $j => $language) {
      if ($j != $language_default->language && $language->enabled == 1) {
        $form['options'][$i]['languages'][$language->language]['language'] = array(
          '#value' =>  $language->native,
        );
        $form['options'][$i]['languages'][$j]['name'] = array(
          '#type' => 'textfield',
          '#default_value' => empty($saved_options[$i][$j]->name) ? $option->name : $saved_options[$i][$j]->name,
        );
  if ($saved_options[$i][$j]->id) {
          $form['options'][$i]['languages'][$j]['id'] = array(
            '#type' => 'hidden',
            '#value' => $saved_options[$i][$j]->id,
          );
        }
      }
    }
  }
  $form['options']['#tree'] = TRUE;
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Format the translation table for options.
 *
 * @ingroup themeable
 */
function theme_uc_i18n_translate_options_form($form) {
  $header = array(' ', array('data' => t('Name'), 'colspan' => 2));
  if (count(element_children($form['options'])) > 0) {
    foreach (element_children($form['options']) as $oid) {
      $row = array(
        drupal_render($form['options'][$oid]['oid']) . drupal_render($form['options'][$oid]['option']),
      );
      $rows[] = array(
        'data' => $row,
        'colspan' => 3,
        'align' => 'center'
      );
      foreach (element_children($form['options'][$oid]['languages']) as $language_id) {
        $row = array(
          drupal_render($form['options'][$oid]['languages'][$language_id]['id']) . drupal_render($form['options'][$oid]['languages'][$language_id]['language']),
          array('data' => drupal_render($form['options'][$oid]['languages'][$language_id]['name']), 'colspan' => 2),
        );
        $rows[] = array(
          'data' => $row,
        );
      }
      $rows[] = array(
        'data' => array(' '),
        'collspan' => 3,
      );
    }
  }
  else {
    $rows[] = array(
      array('data' => t('No translatable options yet.'), 'colspan' => 2),
    );
  }
  $output = theme('table', $header, $rows, array('id' => 'uc-i18n-translate-options-table'));
  $output .= drupal_render($form);
  return $output;
}

/**
 * @see uc_i18n_translate_options_form()
 */
function uc_i18n_translate_options_form_submit($form, &$form_state) {
  $language_default = language_default();
  foreach ($form_state['values']['options'] as $i => $option) {
    foreach ($option['languages'] as $j => $language) {
      $data = array(
        'oid' => $option['oid'],
        'language' => $j,
        'name' => $language['name'],
      );
      if ($j == $language_default->language) {      
        db_query("UPDATE {uc_attribute_options} SET name = '%s' WHERE oid = %d", $language['name'], $option['oid']);
        // temp
        db_query("DELETE IGNORE from {uc_i18n_options} WHERE oid = %d AND language = '%s'", $option['oid'], $j);       
      }
      else {
        if (isset($language['id'])) {
          $data['id'] = $language['id'];
          drupal_write_record('uc_i18n_options', $data, 'id');
        }
        else {
          drupal_write_record('uc_i18n_options', $data);
        }
      }
    }
  }
}

// for the address fields
// see http://drupal.org/node/834290
// fix by brutuscat
// function uc_i18n_uc_store_address_fields() {
//  $form = array();
  //Copied from uc_store_address_fields_form
//  $fields = array(
//    'first_name' => array(t('First name'), TRUE),
//    'last_name' => array(t('Last name'), TRUE),
//    'phone' => array(t('Phone number'), TRUE),
//    'company' => array(t('Company'), TRUE),
//    'street1' => array(t('Street address 1'), TRUE),
//    'street2' => array(t('Street address 2'), TRUE),
//    'city' => array(t('City'), TRUE),
//    'zone' => array(t('State/Province'), TRUE),
//    'country' => array(t('Country'), TRUE),
//    'postal_code' => array(t('Postal code'), TRUE),
//    'address' => array(t('Address'), FALSE),
//    'street' => array(t('Street address'), FALSE),
// );
  //Just loop and build the fields
//  foreach ($fields as $field => $data) {
//    $form['uc_field_' . $field] = array(
//        '#type' => 'textfield',
//        '#title' => $data[0],
//        '#default_value' => variable_get('uc_field_' . $field, $data),
//    );
//  }
  //Magic happens
//  return system_settings_form($form);
//}



