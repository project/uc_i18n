<?php
// $Id$

/**
 * @file
 * Internationalization (i18n) package for Ubercart
 * Additional are also some useful core variables covered.
 *
 */

/**
 * Implementation of hook_menu().
 */
function uc_i18n_menu() {
  $items['admin/store/attributes/translate'] = array(
    'title' => 'Translate attributes',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_i18n_translate_attributes_form'),
    'access arguments' => array('administer attributes'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 7,
    'file' => 'uc_i18n.admin.inc',
  );
  $items['admin/store/attributes/%uc_attribute/options/translate'] = array(
    'title' => 'Translate options',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_i18n_translate_options_form', 3, NULL),
    'access arguments' => array('administer attributes'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 7,
    'file' => 'uc_i18n.admin.inc',
  );
  // address fields
//  $items['admin/store/settings/checkout/edit/fields_lang'] = array(
//    'title' => 'Translate address fields',
//    'description' => 'Translate the address field names',
//    'page callback' => 'drupal_get_form',
//    'page arguments' => array('uc_i18n_uc_store_address_fields'),
//    'access arguments' => array('administer store'),
//    'type' => MENU_LOCAL_TASK,
//    'weight' => 8,
//    'file' => 'uc_i18n.admin.inc',
//  );
  return $items;
}

/**
 * Implementation of hook_theme().
 */
function uc_i18n_theme() {
  return array(
    'uc_i18n_cart_review_table' => array(
      'arguments' => array('show_subtotal' => TRUE),
    ),
    'uc_i18n_translate_attributes_form' => array(
      'arguments' => array('form' => NULL),
      'file' => 'uc_i18n.admin.inc',
    ),
    'uc_i18n_translate_options_form' => array(
      'arguments' => array('form' => NULL),
      'file' => 'uc_i18n.admin.inc',
    ),
  );
}

/**
 * Implementation of hook_nodeapi().
 */
function uc_i18n_nodeapi($node, $op, $arg3 = NULL, $arg4 = NULL) {
  if (in_array($node->type, module_invoke_all('product_types'))) {
    switch ($op) {
      case 'load':
        if ($node->tnid != 0 && $node->nid != $node->tnid) {
          // Add attributes to the node.
          $tnode = node_load($node->tnid);
          if ($tnode->attributes) {
            $t_attributes = uc_i18n_translate_attributes_and_options($tnode->attributes, $node->language);
            $node->attributes = $t_attributes;
          }
        }
      break;
    }
  }
}


/**
 * Implementation of hook_add_to_cart().
 * By firewing1
 */
function uc_i18n_add_to_cart($nid, $qty, $data) {
  /* Due to Drupal's use of multiple nodes for product translations, the same
   * product in a different language is treated as a different product entirely.
   * This is problematic as the same product in different languages can be added
   * to the cart simultaneously. This function works around that problem by
   * always using the tnid/original node. As a result, the cart must be
   * localized as it is displayed.
   */
  $node = node_load($nid);
  // Determine if this node is the source node or a translated one
  // Remember: tnid is 0 if there are no translations
  $is_source = ($node->nid == $node->tnid || $node->tnid == 0) ? 1 : 0;
  if ($is_source) {
    // If it is the source, then all is well�
    $result[] =  array('success' => TRUE);
  } 
  else {
    /* If we are not the source node, then fail to add this product silently and
     * call uc_cart_add_item() to add the source node's product instead. It will
     * be localized later - see uc_i18n_cart_item()
     */
    uc_cart_add_item($node->tnid, $qty, $data);
    $result[] = array('success' => FALSE, 'silent' => TRUE);
  }
  // Remember: We need an array in an array here
  return $result;
}

/**
 * Implementation of hook_cart_item(). 
 * By firewing1
 */
function uc_i18n_cart_item($op, $item) {
  /* hook_cart_display() isn't really a hook, it's mostly for internal use.
   * However, we do need to access later. Setting $item->module forces
   * a module_invoke() call in uc_cart.module to call custommod_cart_display()
   * instead of the default uc_product_cart_display(). We will call
   * uc_product_cart_display() inside our function to ensure things work as
   * usual in future versions.
   */
  $item->module = "uc_i18n_product";
  /* Note that although it is possible to use check for case 'load' in $op and
   * then override the $item->nid and $item->title values, this will cause bugs
   * when attempting to add or remove products in different languages. To
   * resolve these bugs, we are forcing the use of custommod_cart_display() and
   * rewriting the code for the title, img, and anchors to localize the cart.
   */
}

/**
 * Implementation of hook_cart_display().
 * By firewing1
 */
function uc_i18n_product_cart_display($item) {
  /* Call uc_product_cart_display() to get things setup as usual and to ensure
   * this hack still works even if uc_product_cart_display changes at some point
   * in the future.
   */
  $display_item = uc_product_cart_display($item);

  // Get the translations, if any.
  $node = node_load($item->nid);
  global $language;
  $translations = translation_node_get_translations($node->tnid);
  if ($translations[$language->language]) {
    // Reminder: NEVER override the nid. That is what causes the bugs!
    $tnode = node_load($translations[$language->language]->nid);
    $display_item["title"]["#value"] = node_access('view', $tnode) ? l($tnode->title, 'node/'. $tnode->nid) : check_plain($tnode->title);
    $display_item["image"]["#value"] = uc_product_get_picture($tnode->nid, 'cart');
    $display_item["description"]["#value"] = uc_product_get_description($item);
  }
  // if the product is not translated or not in this language, link to product in default language
  else {
    if ($node->language) {
      $languages = language_list();
      $node_language = $languages[$node->language];
    }
    else {
      $node_language = language_default();
    }
    $display_item["title"]["#value"] = node_access('view', $node) ? l($node->title, 'node/'. $node->nid, array('language' => $node_language)) : check_plain($node->title);    
  }

  return $display_item;
}

/**
 * Implementation of hook_checkout_pane(). 
 * By firewing1
 */
function uc_i18n_checkout_pane() {
  /* Replacement for standard cart contents pane. Although hook_cart_item() can
   * be used to localize the checkout pane, then we get into trouble while
   * trying to localize the cart display (see the comments above). The best way
   * that I can think of to work around this is to disable the stock cart
   * contents pane and enable this one instead.
   */
  $panes[] = array(
    'id' => 'uc_i18n_cart',
    'callback' => 'uc_i18n_checkout_pane_cart',
    'title' => t('Your order'),
    'desc' => t('Display the (localized) contents of a customer\'s shopping cart.'),
    'weight' => 2,
    'process' => TRUE,
    'collapsible' => FALSE,
  );
  return $panes;
}

/**
 * Callback for our implementation of hook_checkout_pane().
 * By firewing1
 */
function uc_i18n_checkout_pane_cart($op, &$arg1, $arg2) {
  /* The code below is copied from uc_checkout_pane_cart() and is slightly
   * modified to localize the cart contents before displaying it. If you are
   * using this, you need to keep an eye on uc_checkout_pane_cart() to make sure
   * that if there is an important change or bugfix, you make the same change
   * here.
   */

  switch ($op) {
    case 'view':
      $contents['cart_review_table'] = array(
        '#value' => theme('uc_i18n_cart_review_table'),
        '#weight' => variable_get('uc_pane_cart_field_cart_weight', 2),
      );
      return array('contents' => $contents, 'next-button' => FALSE);
//    break;  
    case 'review':
      $items = uc_cart_get_contents();

      $output = '<table>';
      $context = array(
        'revision' => 'themed',
        'type' => 'cart_item',
        'subject' => array(),
      );
      global $language;
      foreach ($items as $item) {
        $node = node_load($item->nid);
        $translations = translation_node_get_translations($node->tnid);
        if ($translations[$language->language]) {
          $tnode = node_load($translations[$language->language]->nid);
        } 
        else {
          $tnode = $node;
        }
        $desc = check_plain($tnode->title) . uc_product_get_description($item);
        $price_info = array(
          'price' => $item->price,
          'qty' => $item->qty,
        );
        $context['subject'] = array(
          'cart' => $items,
          'cart_item' => $item,
          'node' => $tnode,
        );
        $output .= '<tr valign="top"><td>'. $item->qty .'&times;</td><td width="100%">'. $desc
                  .'</td><td nowrap="nowrap">'. uc_price($price_info, $context) .'</td></tr>';
      }
      $output .= '</table>';
      $review[] = $output;
      return $review;
  }
  return $result;
}

function uc_i18n_translate_option($oid, $language) {
  $result = db_query("SELECT * FROM {uc_i18n_options} WHERE oid = %d AND language = '%s'", $oid, $language);
  while ($options = db_fetch_object($result)) {
    $option = $options->name;
  }
  return $option;
}

function uc_i18n_translate_attribute($aid, $language) {
  $result = db_query("SELECT * FROM {uc_i18n_attributes} WHERE aid = %d AND language = '%s'", $aid, $language);
  return db_fetch_object($result); 
}

function uc_i18n_translate_attributes_and_options($attributes, $language) {
  $aids = array();
  $oids = array();
  foreach ($attributes As $i => $attribute) {
    $aids[] = $i;
    foreach ($attribute->options AS $j => $option) {
      $oids[] = $j;
    }
  }

  if ($aids) {
    // Translate all attributes 
    $attributes_result = db_query("
      SELECT * FROM {uc_i18n_attributes} WHERE aid IN 
      (" . implode(",", array_filter($aids, 'is_numeric')) . ")
      AND language = '%s'", $language);
    while ($t_attribute = db_fetch_object($attributes_result)) {
      $attributes[$t_attribute->aid]->label = $t_attribute->label ? $t_attribute->label : $attributes[$t_attribute->aid]->label;
      $attributes[$t_attribute->aid]->description = $t_attribute->description ? $t_attribute->description : $attributes[$t_attribute->aid]->description;
    }
  }

  if ($oids) {
    // Translate all options  
    $t_options = array();
    $options_result = db_query("
      SELECT * FROM {uc_i18n_options} WHERE oid IN 
      (" . implode(",", array_filter($oids, 'is_numeric')) . ") 
      AND language = '%s'", $language);
    while ($t_option = db_fetch_object($options_result)) {
      $t_options[$t_option->oid] = $t_option;
    }
    foreach ($attributes As $i => $attribute) {
      foreach ($attribute->options AS $j => $option) {
        $attributes[$i]->options[$j]->name = $t_options[$j]->name ? $t_options[$j]->name : $attributes[$i]->options[$j]->name;
      }
    }
  }
  return $attributes;
}

function uc_i18n_order($op, $arg1, $arg2) {

  GLOBAL $language;
  $default_language = language_default();
  
 
  switch ($op) {
    case 'presave':    
      foreach ($arg1->products AS $i => $product) {
        $node = node_load($product->nid, NULL, TRUE);
        if ($node->tnid) {
          $translations = translation_node_get_translations($node->tnid);
          if ($translations[$default_language->language]->nid != $node->nid) {
            $node = node_load($translations[$default_language->language]->nid, NULL, TRUE);
          } 
          elseif ($node->nid != $node->tnid) {
            $node = node_load($node->tnid, NULL, TRUE);
          }
          if ($translations[$language->language]) {
            $tnode = node_load($translations[$language->language]->nid, NULL, TRUE);
          }
          else {
            $tnode = $node;
          }
        }
        $arg1->products[$i]->l_title = $tnode->title;
        $arg1->products[$i]->extra_data = array();
        $arg1->products[$i]->l_data = array();
        $new_attributes = array();
        foreach ($product->data['attributes'] AS $j => $options) {
          $extra_attribute = array(
            'name' => $node->attributes[$j]->name,
            'label' => $node->attributes[$j]->label,
            'id' => $j,
          );
          $l_attribute = array(
            'name' => $tnode->attributes[$j]->name,
            'label' => $tnode->attributes[$j]->label,
            'id' => $j,
          );

          if (empty($node->attributes[$j]->options)) {
            // text attributes don't have options
            $extra_attribute['options'][0] = array(
              'name' => $options,
              'id' => 0,
            );
            $new_attributes[$node->attributes[$j]->label][] = $options;
            $l_attribute['options'][0] = array(
              'name' => $options,
              'id' => 0,
            );

          }
          elseif (is_array($options)) {
            // checkboxes can have multiple options
            $extra_attribute['options'] = array();
            foreach ($options AS $y => $option) {
              if ($option) {
               $extra_attribute['options'][$y] = array(
                 'name' => $node->attributes[$j]->options[$y]->name,
                 'id' => $y,
               );
               $new_attributes[$node->attributes[$j]->label][] = $node->attributes[$j]->options[$y]->name;
               $l_attribute['options'][$y] = array(
                 'name' => $tnode->attributes[$j]->options[$y]->name,
                 'id' => $y,
               );
              }
            }
          }
          elseif (is_numeric($options)) {
            // dropdown lists have only 1
            $extra_attribute['options'][$options] = array(
              'name' => $node->attributes[$j]->options[$options]->name,
              'id' => $options,
            );
            $new_attributes[$node->attributes[$j]->label][] = $node->attributes[$j]->options[$options]->name;
            $l_attribute['options'][$options] = array(
              'name' => $tnode->attributes[$j]->options[$options]->name,
              'id' => $options,
            );
          }
          $arg1->products[$i]->extra_data['attributes'][$j] = $extra_attribute;
          $arg1->products[$i]->l_data['attributes'][$j] = $l_attribute;
        }
//        $arg1->products[$i]->data['attributes'] = $new_attributes;
      }
      break;

    case 'save':
      $order_language = $arg1->language ? $arg1->language : $language->language;
      db_query("UPDATE {uc_i18n_orders} SET language = '%s' WHERE order_id = %d", $order_language, $arg1->order_id);
      if (db_affected_rows() == 0) {
        db_query("INSERT INTO {uc_i18n_orders} (order_id, language) VALUES (%d, '%s')", $arg1->order_id, $order_language);
      }
      
      foreach ($arg1->products AS $i => $product) {
        $l_data = serialize($product->l_data);
        $extra_data = serialize($product->extra_data);
        db_query("UPDATE {uc_i18n_order_products} SET l_title = '%s', l_data = '%s', extra_data = '%s' WHERE order_product_id = %d", $product->l_title, $l_data, $extra_data, $product->order_product_id);
        if (db_affected_rows() == 0) {
          db_query("INSERT INTO {uc_i18n_order_products} (order_product_id, l_title, l_data, extra_data) VALUES (%d, '%s', '%s', '%s')", $product->order_product_id, $product->l_title, $l_data, $extra_data);
        }
      }

      break;
    case 'load':
      $result = db_fetch_object(db_query("SELECT language FROM {uc_i18n_orders} WHERE order_id = %d", $arg1->order_id));
      if ($result) {
        $arg1->language = $result->language;
      }
     foreach ($arg1->products AS $i => $product) {
        $result = db_fetch_object(db_query("SELECT * FROM {uc_i18n_order_products} WHERE order_product_id = %d", $product->order_product_id));
        if ($result) {
          $arg1->products[$i]->l_title = $result->l_title;
          $arg1->products[$i]->l_data = unserialize($result->l_data);
          $arg1->products[$i]->extra_data = unserialize($result->extra_data);          
        }
      }

      break;

    case 'delete':
      db_query("DELETE FROM {uc_i18n_orders} WHERE order_id = %d", $arg1->order_id);
/*
      foreach ($arg1->products AS $i => $product) {
        db_query("DELETE FROM {uc_i18n_order_products} WHERE order_product_id = %d", $product->order_product_id);
      }
*/
      break;
  }
}


/******************************************************************************
 * Module Functions                                                           *
 ******************************************************************************/

/**
 * Load attributes from the database.
 *
 */
function uc_i18n_attribute_load() {
  $attributes = array();
  $result = db_query("SELECT * FROM {uc_attributes}");
  while ($attribute = db_fetch_object($result)) {
    $attributes[$attribute->aid] = $attribute;
  }
  return $attributes;
}


/**
 * Implementation of hook_i18nsync_fields_alter().
 */
function uc_i18n_i18nsync_fields_alter($fields, $type) {
  if (in_array($type, uc_product_types())) {
    $fields['uc_products']['#title'] = 'Products';
    // These values were found by doing a print_r($node) in node-product.tpl.php
    $fields['uc_products']['#options'] = array(
      'model' => 'SKU',
      'list_price' => 'List Price',
      'cost' => 'Cost',
      'sell_price' => 'Sell Price',
      'weight' => 'Weight',
      'weight_units' => 'Weight Units',
      'length' => 'Length',
      'width' => 'Width',
      'dim_height' => 'Dim_Height',
      'dim_length' => 'Dim_Length',
      'dim_width' => 'Dim_Width',
      'height' => 'Height',
      'length_units' => 'Length Units',
      'pkg_qty' => 'Quantity',
      'default_qty' => 'Default quantity to add to cart',
      'ordering' => 'List position',
      'shippable' => 'Item is shippable',
    );
  }
}

/**
 * Implementation of hook_init().
 */
function uc_i18n_init() {
  global $conf;
  $add_conf['i18n_variables'] = array(
    // Site configuration
    'site_name',
    'site_slogan',
    'site_mission',
    'site_footer',
    'site_frontpage',
    'anonymous',
    'uc_sign_after_amount',
  );
  if ($conf['i18n_variables']) {
    $conf['i18n_variables'] = array_merge($conf['i18n_variables'], $add_conf['i18n_variables']);
  }
  else {
    $conf['i18n_variables'] = $add_conf['i18n_variables'];
  }
}

/**
 * Format the cart contents table on the checkout page.
 *
 * @param $show_subtotal
 *   TRUE or FALSE indicating if you want a subtotal row displayed in the table.
 * @return
 *   The HTML output for the cart review table.
 *
 * @ingroup themeable
 */
function theme_uc_i18n_cart_review_table($show_subtotal = TRUE) {
  $subtotal = 0;
  global $language;
  // Set up table header.
  $header = array(
    array('data' => t('Qty'), 'class' => 'qty'),
    array('data' => t('Products'), 'class' => 'products'),
    array('data' => t('Price'), 'class' => 'price'),
  );

  $context = array();

  // Set up table rows.
  $contents = uc_cart_get_contents();

  foreach ($contents as $item) {
    
    $node = node_load($item->nid);
    $translations = translation_node_get_translations($node->tnid);
    if ($translations[$language->language]) {
      $tnode = node_load($translations[$language->language]->nid);
    }
    else {
      $tnode = $node;
    }

    $price_info = array(
      'price' => $item->price,
      'qty' => $item->qty,
    );

    $context['revision'] = 'altered';
    $context['type'] = 'cart_item';
    $context['subject'] = array(
      'cart' => $contents,
      'cart_item' => $item,
      'node' => $node,
    );

    $total = uc_price($price_info, $context);
    $subtotal += $total;

    $description = check_plain($tnode->title) . uc_product_get_description($item);

    // Remove node from context to prevent the price from being altered.
    $context['revision'] = 'themed-original';
    $context['type'] = 'amount';
    unset($context['subject']);
    $rows[] = array(
      array('data' => t('@qty&times;', array('@qty' => $item->qty)), 'class' => 'qty'),
      array('data' => $description, 'class' => 'products'),
      array('data' => uc_price($total, $context), 'class' => 'price'),
    );
  }

  // Add the subtotal as the final row.
  if ($show_subtotal) {
    $context = array(
      'revision' => 'themed-original',
      'type' => 'amount',
    );
    $rows[] = array(
      'data' => array(array('data' => '<span id="subtotal-title">' . t('Subtotal:') . '</span> ' . uc_price($subtotal, $context), 'colspan' => 3, 'class' => 'subtotal')),
      'class' => 'subtotal',
    );
  }

  return theme('table', $header, $rows, array('class' => 'cart-review'));
}
